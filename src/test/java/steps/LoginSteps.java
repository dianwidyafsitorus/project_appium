package steps;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.LoginPO;
import utilities.ThreadManager;

public class LoginSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private LoginPO login = new LoginPO(driver);

    @When("user input valid Email login {string}")
    public void user_input_valid_Email(String email) throws InterruptedException {
        login.enterEmailLogin(email);
    }

    @When("user input password login {string}")
    public void user_input_password(String password) {
        login.enterPasswordLogin(password);
    }

    @When("user click button Login")
    public void user_click_button_Register() {
        login.clickOnButtonLogin();
    }

    @Then("system display message login {string}")
    public void system_display_message(String msgSuccess) throws InterruptedException {
        Assert.assertEquals(login.displayLoginSuccess(), msgSuccess, "not matched");
    }

    @Then("system display error message login")
    public void system_display_error_message() {
        login.displayLoginError();
    }
}
