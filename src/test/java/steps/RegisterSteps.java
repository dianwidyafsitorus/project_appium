package steps;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.RegisterPO;
import utilities.ThreadManager;

public class RegisterSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private RegisterPO register = new RegisterPO(driver);

    @Given("user click No account yet? create one")
    public void user_click_No_account_yet_create_one() {
        register.clickOnLinkRegister();
    }

    @When("user input valid Name {string}")
    public void user_input_valid_Name(String name) {
        register.enterName(name);
    }

    @When("user input valid Email {string}")
    public void user_input_valid_Email(String email) throws InterruptedException {
        register.enterEmail(email);
    }

    @When("user input password {string}")
    public void user_input_password(String password) {
        register.enterPassword(password);
    }

    @When("user input confirm password {string}")
    public void user_input_confirm_password(String confirmPassword) {
        register.enterConfirmPassword(confirmPassword);
    }

    @When("user click button Register")
    public void user_click_button_Register() {
        register.clickOnButtonRegister();
    }

    @Then("system display message {string}")
    public void system_display_message(String msgSuccess) throws InterruptedException {
        Assert.assertEquals(register.displayRegisterSuccess(), msgSuccess, "not matched");
    }

    @Then("system display error message")
    public void system_display_error_message() {
        register.displayRegisterError();
    }

}
