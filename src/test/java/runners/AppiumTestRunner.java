package runners;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/register/cucumber-report.json",  "html:target/results/register"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@appium"}

)

public class AppiumTestRunner extends BaseTestRunnerAndroid{

}
