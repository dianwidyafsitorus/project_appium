@regression @appium

Feature: Register

  Scenario: Positive case Register
    Given user click No account yet? create one
    When user input valid Name "Dian"
    And user input valid Email "dwfssito@gmail.com"
    And user input password "februari11"
    And user input confirm password "februari11"
    And user click button Register
    Then system display message "Registration Successful"

  Scenario Outline: Negative case Register
    Given user click No account yet? create one
    When user input valid Name <Name>
    And user input valid Email <Email>
    And user input password <Password>
    And user input confirm password <confirmPassword>
    And user click button Register
    Then system display error message

    Examples:
      | Name                 | Email              | Password             | confirmPassword     |
      | ""                   | ""                 | ""                   | ""                  |
      | "nama"               | ""                 |""                    | ""                  |
      | ""                   | "dwfssi@gmail.com" | "februar"            | "februar"           |
