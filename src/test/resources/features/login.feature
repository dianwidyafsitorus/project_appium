@regression @appium

Feature: Login

  Scenario: Positive case Login
    When user input valid Email login "dwfssito@gmail.com"
    And user input password login "februari11"
    And user click button Login
    Then system display message login "Wrong Email or Password"

  Scenario Outline: Negative case Login
    When user input valid Email login <Email>
    And user input password login <Password>
    And user click button Login
    Then system display error message login

    Examples:
      | Email              | Password             |
      | ""                 | ""                   |
      | "dwfs@gmail"       | ""                   |
      | "dwfssi@gmail.com" | "februar"            |
