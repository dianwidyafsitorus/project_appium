package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class RegisterPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public RegisterPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "textViewLinkRegister")
    private WebElement linkRegister;

    @AndroidFindBy(id = "textInputEditTextName")
    private WebElement namedEditText;

    @AndroidFindBy(id = "textInputEditTextEmail")
    private WebElement emailEditText;

    @AndroidFindBy(id = "textInputEditTextPassword")
    private WebElement passwordEditText;

    @AndroidFindBy(id = "textInputEditTextConfirmPassword")
    private WebElement confirmPasswordEditText;

    @AndroidFindBy(id = "appCompatButtonRegister")
    private WebElement registerButton;

    @AndroidFindBy(id = "snackbar_text")
    private WebElement msgRegister;



    /**
     * Click on Link Register
     */
    public void clickOnLinkRegister() {
        linkRegister.click();
    }

    /**
     * Enter Name
     * @param name
     */
    public void enterName(String name){
        appium.enterText(namedEditText, name, true);
    }

    /**
     * Enter Email
     * @param email
     */
    public void enterEmail(String email) throws InterruptedException {
        appium.hardWait(3);
        appium.enterText(emailEditText, email, true);
    }

    /**
     * Enter Email
     * @param password
     */
    public void enterPassword(String password){
        appium.enterText(passwordEditText, password, true);
    }

    /**
     * Enter Email
     * @param confirmPassword
     */
    public void enterConfirmPassword(String confirmPassword){
        appium.enterText(confirmPasswordEditText, confirmPassword, true);
    }

    /**
     * Click on Button Register
     */
    public void clickOnButtonRegister() {
        registerButton.click();
    }

    /**
     * Verify Register success
     * @throws InterruptedException
     */
    public String displayRegisterSuccess(){
        appium.waitTillElementIsVisible(msgRegister);
        return appium.getText(msgRegister);
    }

    /**
     * Verify Register error
     * @throws InterruptedException
     */
    public void displayRegisterError(){
        appium.isElementDisplayed(msgRegister);
    }


}
