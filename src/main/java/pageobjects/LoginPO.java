package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class LoginPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public LoginPO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "textInputEditTextEmail")
    private WebElement emailEditText;

    @AndroidFindBy(id = "textInputEditTextPassword")
    private WebElement passwordEditText;

    @AndroidFindBy(id = "appCompatButtonLogin")
    private WebElement loginButton;

    @AndroidFindBy(id = "snackbar_text")
    private WebElement msgLogin;


    /**
     * Enter Email
     * @param email
     */
    public void enterEmailLogin(String email) throws InterruptedException {
        appium.hardWait(3);
        appium.enterText(emailEditText, email, true);
    }

    /**
     * Enter Email
     * @param password
     */
    public void enterPasswordLogin(String password){
        appium.enterText(passwordEditText, password, true);
    }

    /**
     * Click on Button Register
     */
    public void clickOnButtonLogin() {
        loginButton.click();
    }

    public String displayLoginSuccess(){
        appium.waitTillElementIsVisible(msgLogin);
        return appium.getText(msgLogin);
    }

    /**
     * Verify Register error
     * @throws InterruptedException
     */
    public void displayLoginError(){
        appium.isElementDisplayed(msgLogin);
    }
}
